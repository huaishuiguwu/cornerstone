package com.cs.micro.demo.quartz2.config;

import com.cs.base.common.SpringBootContext;
import com.cs.base.quartz.core.DetailJobBean;
import com.cs.micro.demo.quartz2.task.DemoTask;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 * @author wangjiahao
 * @version 1.0
 * @className QuartzConfig
 * @since 2019-03-18 09:51
 */
@Slf4j
@Configuration
public class QuartzConfig {

    @Autowired
    private ApplicationContext context;

    @Bean
    public SpringBootContext springBootContext() {
        return new SpringBootContext();
    }


    @Bean
    public JobDetailFactoryBean jobDetailFactoryBean() {
        JobDetailFactoryBean bean = new JobDetailFactoryBean();
        bean.setJobClass(new DetailJobBean().getClass());
        bean.setDurability(true);
        bean.setRequestsRecovery(true);
        bean.setApplicationContext(context);

        return bean;
    }

    @Bean
    public CronTriggerFactoryBean demoTaskTrigger1(JobDetailFactoryBean jobDetailFactoryBean) {
        CronTriggerFactoryBean bean = new CronTriggerFactoryBean();

        JobDataMap map = new JobDataMap();
        map.put("targetObject", "demoTask");
        map.put("targetMethod", "execute");
        map.put("concurrent", "false");
        bean.setJobDataMap(map);
        jobDetailFactoryBean.setJobDataMap(map);

        bean.setJobDetail(jobDetailFactoryBean.getObject());
        bean.setCronExpression("*/5 * * * * ? ");

        return bean;
    }

    @Bean
    public CronTriggerFactoryBean demoTaskTrigger2(JobDetailFactoryBean jobDetailFactoryBean) {
        CronTriggerFactoryBean bean = new CronTriggerFactoryBean();

        JobDataMap map = new JobDataMap();
        map.put("targetObject", "demoTask2");
        map.put("targetMethod", "execute");
        map.put("concurrent", "false");
        bean.setJobDataMap(map);
        jobDetailFactoryBean.setJobDataMap(map);

        bean.setJobDetail(jobDetailFactoryBean.getObject());
        bean.setCronExpression("* * * * * ? ");

        return bean;
    }
}
