package com.cs.micro.demo.quartz1.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author wangjiahao
 * @version 1.0
 * @className LocalTask
 * @since 2019-03-19 17:48
 */
@Component
public class LocalTask {

    @Scheduled(cron = "*/5 * * * * ? ")
    public void test() {
        System.err.println("本地测试定时任务");
    }

}
